﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Services.ViewModels
{
    public class AuthorViewModel
    {
        public int? Id { get; set; }

        public string Fullname { get; set; }
    }
}
