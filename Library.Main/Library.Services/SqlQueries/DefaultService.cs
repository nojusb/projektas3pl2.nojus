﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Library.Services.SqlQueries
{
    public class DefaultService<T> where T : class
    {
        public MySqlConnection connection { get; set; }
        private string _connectionString = "server=localhost;uid=root;pwd=root;database=library";
        public DefaultService()
        {
            connection = new MySqlConnection(_connectionString);
        }

        
    }
}
