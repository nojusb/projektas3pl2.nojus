﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Models
{
    public class Genre
    {
        public int? GenreId { get; set; }

        public string Name { get; set; }

        private Genre()
        {

        }

        public Genre(string name, int? genreId = null)
        {
            GenreId = genreId;
            Name = name;
        }
    }
}
